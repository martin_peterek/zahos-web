<?php

// Define some constants
define( "RECIPIENT_NAME", "Michal Hos" );
define( "RECIPIENT_EMAIL", "michal.hos@seznam.cz" );


// Read the form values
$success = false;
$senderEmail = isset( $_POST['email'] ) ? preg_replace( "/[^\.\-\_\@a-zA-Z0-9]/", "", $_POST['email'] ) : "";
$senderPhone = isset( $_POST['telefon'] ) ? preg_replace( "/[^\.\-\_\@a-zA-Z0-9]/", "", $_POST['telefon'] ) : "";
$manufacturer = isset( $_POST['manufacturer'] ) ? preg_replace( "/[^\.\-\_\@a-zA-Z0-9]/", "", $_POST['manufacturer'] ) : "";
$model = isset( $_POST['model'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['model'] ) : "";
$volume = isset( $_POST['volume'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['volume'] ) : "";
$year = isset( $_POST['year'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['year'] ) : "";
$mileage = isset( $_POST['mileage'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['mileage'] ) : "";
$money = isset( $_POST['money'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['money'] ) : "";

// If all values exist, send the email
if ($senderEmail && $senderPhone) {
  $recipient = RECIPIENT_NAME . " <" . RECIPIENT_EMAIL . ">";
  $headers = "Od: " . $senderEmail . "";
  $msgBody = "Email: ". $senderEmail . "\n". 
  "Telefon: ". $telefon . "\n".
  "Výrobce: ". $manufacturer .  "\n".
  "Model: " . $model . "\n".
  "Objem motoru: " . $volume . "\n".
  "Rok výroby: " . $year . "\n".
  "Aktuální stav tachometru: " . $mileage . "\n".
  "Požadovaná částka: " . $money;
  $headersEnd = "From: $senderEmail\r\nReply-to: $senderEmail";
  $success = mail( $recipient, $headers, $msgBody, $headersEnd);

  //Set Location After Successsfull Submission
  header('Location: index.html?message=Successfull');
}

else{
	//Set Location After Unsuccesssfull Submission
  	header('Location: index.html?message=Failed');	
}

?>
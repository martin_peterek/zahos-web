function menuScroll(param) {
  var id = '#' + param;
  console.log(id);
  $(id).addClass('current');
}

$('li[id*="#"], section[id*="#"]').click(function () {
  var link = $(this).attr('id').replace('#', '');
  var tempLink = '#' + link.replace('Link', '');
  menuScroll(link)
  if (tempLink === '#home' || tempLink === '#aboutUs' || tempLink === '#services' || tempLink === '#contact') {
    $('html, body').animate({
      scrollTop: $(tempLink).offset().top
    }, 500);
  } else {
    return;
  }
});

$(document).ready(function () {
  $('#submitForm').attr('disabled', true);
  $('#agree').change(function () {
    if ($(this).is(':checked')) {
      $('#submitForm').attr('disabled', false);
    } else {
      $('#submitForm').attr('disabled', true);
    }
    console.log($('#agree').is(':checked'));
  });

});